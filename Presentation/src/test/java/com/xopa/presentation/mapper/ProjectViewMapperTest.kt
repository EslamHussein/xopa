package com.xopa.presentation.mapper

import com.xopa.presentation.factory.ProjectFactory
import org.junit.Assert
import org.junit.Test


class ProjectViewMapperTest {

    private val mapper = ProjectViewMapper()

    @Test
    fun mapToView() {
        val project = ProjectFactory.makeProject()
        val projectView = mapper.mapToView(project)

        Assert.assertEquals(project.id, projectView.id)
        Assert.assertEquals(project.name, projectView.name)

    }
}