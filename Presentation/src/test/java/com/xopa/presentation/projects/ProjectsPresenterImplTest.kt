package com.xopa.presentation.projects

import com.nhaarman.mockito_kotlin.*
import com.xopa.domain.interactor.search.GetProjects
import com.xopa.domain.model.Project
import com.xopa.presentation.factory.ProjectFactory
import com.xopa.presentation.mapper.ProjectViewMapper
import io.reactivex.observers.DisposableObserver
import org.junit.Before
import org.junit.Test
import org.junit.runner.RunWith
import org.junit.runners.JUnit4
import org.mockito.Captor

@RunWith(JUnit4::class)
class ProjectsPresenterImplTest {

    @Captor
    private val captor = argumentCaptor<DisposableObserver<List<Project>>>()


    private val getProjects = mock<GetProjects>()
    private val mapper = mock<ProjectViewMapper>()
    private val view = mock<ProjectsContract.ProjectsView>()

    private lateinit var projectsPresenterImpl: ProjectsPresenterImpl

    @Before
    fun setup() {
        projectsPresenterImpl = ProjectsPresenterImpl(getProjects, mapper)
        projectsPresenterImpl.onAttach(view)
    }

    @Test
    fun getProjectsCompletes() {

        val projects = ProjectFactory.makeProjectList(1)
        val projectViews = ProjectFactory.makeProjectViewList(1)

        whenever(mapper.mapToView(projects.first()))
                .thenReturn(projectViews.first())

        projectsPresenterImpl.searchTrending(0)


        verify(view).showLoading()
        verify(getProjects).execute(captor.capture(), eq(null))
        captor.firstValue.onNext(projects)
        verify(view).showSearchResultSuccess(projectViews, any())
        captor.lastValue.onComplete()
        verify(view).hideLoading()

    }

}