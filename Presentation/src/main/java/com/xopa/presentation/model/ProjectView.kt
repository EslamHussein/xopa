package com.xopa.presentation.model

import android.os.Parcelable
import kotlinx.android.parcel.Parcelize

@Parcelize
data class ProjectView(val id: Int, val name: String, val ownerName: String,
                       val avatarUrl: String, val description: String?,
                       val watchersCount: Int, val stargazersCount: Int,
                       val language: String?, val forksCount: Int, val url: String) : Parcelable