package com.xopa.presentation.mapper

interface Mapper<V, D> {
    fun mapToView(type: D): V
}