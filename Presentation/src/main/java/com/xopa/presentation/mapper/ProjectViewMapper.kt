package com.xopa.presentation.mapper

import com.xopa.domain.model.Project
import com.xopa.presentation.model.ProjectView
import javax.inject.Inject

open class ProjectViewMapper @Inject constructor() : Mapper<ProjectView, Project> {
    override fun mapToView(type: Project): ProjectView = ProjectView(type.id, type.name,type.ownerName,type.avatarUrl
            ,type.description,type.watchersCount,type.stargazersCount,type.language,
            type.forksCount,type.url)
}