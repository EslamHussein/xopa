package com.xopa.presentation.base

interface MvpPresenter<V : MvpView> {


    fun onAttach(view: V)

    fun onResume()

    fun onDetach()

}