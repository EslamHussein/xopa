package com.xopa.presentation.projects

import com.xopa.domain.interactor.search.GetProjects
import com.xopa.domain.model.Project
import com.xopa.presentation.mapper.ProjectViewMapper
import io.reactivex.observers.DisposableObserver
import javax.inject.Inject

class ProjectsPresenterImpl @Inject constructor(private val getProjects: GetProjects?,
                                                private val mapper: ProjectViewMapper) : ProjectsContract.ProjectsPresenter() {
    override fun searchTrending(pageNumber: Int) {
        view?.showLoading()
        getProjects?.execute(ProjectsSubscriber(), GetProjects.Params.forProjects("android", "stars", "desc", pageNumber))

    }


    override fun onDetach() {
        super.onDetach()
        getProjects?.dispose()

    }


    inner class ProjectsSubscriber @Inject constructor() : DisposableObserver<List<Project>>() {
        override fun onNext(t: List<Project>) {

            view?.showSearchResultSuccess(t.map {
                mapper.mapToView(it)
            })

        }

        override fun onComplete() {
            view?.hideLoading()

        }

        override fun onError(e: Throwable) {
            view?.showSearchResultFailure(e.localizedMessage)
            view?.hideLoading()


        }

    }
}