package com.xopa.presentation.projects

import com.xopa.presentation.base.BasePresenter
import com.xopa.presentation.base.MvpView
import com.xopa.presentation.model.ProjectView

interface ProjectsContract {

    interface ProjectsView : MvpView {

        fun showLoading()
        fun hideLoading()
        fun showSearchResultSuccess(projects: List<ProjectView>, firstVisibleCellIndex: Int = 0)
        fun showSearchResultFailure(msg: String)
    }

    abstract class ProjectsPresenter : BasePresenter<ProjectsView>() {
        abstract fun searchTrending(pageNumber: Int)
    }
}