package com.xopa.domain.model

/**
 * Created by Eslam Hussein on 8/27/18.
 */
data class Project(val id: Int, val name: String, val ownerName: String,
                   val avatarUrl: String, val description: String?,
                   val watchersCount: Int, val stargazersCount: Int,
                   val language: String?, val forksCount: Int, val url: String)