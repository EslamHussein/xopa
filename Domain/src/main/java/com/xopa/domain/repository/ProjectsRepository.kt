package com.xopa.domain.repository

import com.xopa.domain.interactor.search.GetProjects
import com.xopa.domain.model.Project
import io.reactivex.Observable

/**
 * Created by Eslam Hussein on 8/27/18.
 */
interface ProjectsRepository {


    fun getProjects(params: GetProjects.Params): Observable<List<Project>>
}