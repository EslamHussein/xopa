package com.xopa.domain.interactor.search

import com.xopa.domain.executer.ExecutionThread
import com.xopa.domain.interactor.ObservableUseCase
import com.xopa.domain.model.Project
import com.xopa.domain.repository.ProjectsRepository
import io.reactivex.Observable
import javax.inject.Inject

/**
 * Created by Eslam Hussein on 8/27/18.
 */
open class GetProjects @Inject constructor(private val projectsRepository: ProjectsRepository,
                                           executionThread: ExecutionThread) :
        ObservableUseCase<List<Project>, GetProjects.Params>(executionThread) {
    override fun buildUseCaseObservable(params: Params?): Observable<List<Project>> {

        if (params == null) throw IllegalArgumentException("Params can't be null")

        return projectsRepository.getProjects(params)

    }


    data class Params(val q: String, val sort: String, val order: String, val page: Int) {

        companion object {

            fun forProjects(q: String, sort: String, order: String, page: Int): Params {
                return Params(q, sort, order, page)
            }
        }
    }
}