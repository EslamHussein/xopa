package com.xopa.domain.interactor

import com.xopa.domain.executer.ExecutionThread
import io.reactivex.Observable
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.disposables.Disposable
import io.reactivex.observers.DisposableObserver

/**
 * Created by Eslam Hussein on 8/27/18.
 */
abstract class ObservableUseCase<T, in Params> constructor(private val execution: ExecutionThread) {


    private val disposables = CompositeDisposable()
    abstract fun buildUseCaseObservable(params: Params? = null): Observable<T>

    open fun execute(observer: DisposableObserver<T>, params: Params? = null) {

        val observable = this.buildUseCaseObservable(params = params)
                .subscribeOn(execution.subscribeScheduler)
                .observeOn(execution.observerScheduler)
        addDisposable(observable.subscribeWith(observer))
    }

    fun dispose() {
        disposables.dispose()
    }

    private fun addDisposable(disposable: Disposable) {
        disposables.add(disposable)
    }


}