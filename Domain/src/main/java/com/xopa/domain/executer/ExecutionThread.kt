package com.xopa.domain.executer

import io.reactivex.Scheduler

/**
 * Created by Eslam Hussein on 8/27/18.
 */
interface ExecutionThread {
    val subscribeScheduler: Scheduler
    val observerScheduler: Scheduler
}