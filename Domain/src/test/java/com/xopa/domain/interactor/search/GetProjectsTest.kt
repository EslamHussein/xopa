package com.xopa.domain.interactor.search

import com.nhaarman.mockito_kotlin.whenever
import com.xopa.domain.executer.ExecutionThread
import com.xopa.domain.model.Project
import com.xopa.domain.repository.ProjectsRepository
import com.xopa.domain.test.ProjectDataFactory
import io.reactivex.Observable
import org.junit.Before
import org.junit.Test
import org.mockito.Mock
import org.mockito.Mockito
import org.mockito.MockitoAnnotations
import java.util.*


/**
 * Created by Eslam Hussein on 8/27/18.
 */
class GetProjectsTest {


    private lateinit var getProjects: GetProjects
    @Mock
    lateinit var projectsRepository: ProjectsRepository
    @Mock
    lateinit var executionThread: ExecutionThread

    @Before
    fun setup() {

        MockitoAnnotations.initMocks(this)
        getProjects = GetProjects(projectsRepository, executionThread)
    }

    @Test
    fun getProjectsSuccess() {

        val result: List<Project> = ProjectDataFactory.generateProjectList(5)

        // Given

        val testParam = GetProjects.Params.forProjects("a", "a", "s", 1)


        whenever(projectsRepository.getProjects(testParam))
                .thenReturn(Observable.just(result))

        // When
        val testObservable = getProjects.buildUseCaseObservable(testParam).test()

        // Then
        testObservable.assertComplete()

    }

    @Test(expected = IllegalArgumentException::class)
    fun getProjectsFailureWithNullParams() {

        val result: List<Project> = ProjectDataFactory.generateProjectList(5)

        // Given

        val testParam = GetProjects.Params.forProjects("a", "a", "s",1)


        whenever(projectsRepository.getProjects(testParam))
                .thenReturn(Observable.just(result))

        // When
        val testObservable = getProjects.buildUseCaseObservable().test()

        // Then
        testObservable.assertComplete()

    }

}