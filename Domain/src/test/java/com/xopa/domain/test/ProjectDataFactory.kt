package com.xopa.domain.test

import com.xopa.domain.model.Project
import java.util.*
import java.util.concurrent.ThreadLocalRandom

/**
 * Created by Eslam Hussein on 8/27/18.
 */
object ProjectDataFactory {


    private fun randomString() = UUID.randomUUID().toString()
    fun randomInt(): Int {
        return ThreadLocalRandom.current().nextInt(0, 1000 + 1)
    }

    fun generateProjectList(count: Int): List<Project> {

        val projects = mutableListOf<Project>()
        repeat(count) {

            projects.add(Project(randomInt(), randomString(), randomString(), randomString(),
                    randomString(), randomInt(), randomInt(), randomString(), randomInt(), randomString()))
        }
        return projects

    }
}