package com.xopa.data.mapper

import com.xopa.data.model.ProjectEntity
import com.xopa.domain.model.Project
import javax.inject.Inject

open class ProjectMapper @Inject constructor() : EntityMapper<ProjectEntity, Project> {
    override fun mapFromEntity(entity: ProjectEntity): Project {

        return Project(entity.id, entity.name,entity.ownerName,
                entity.avatarUrl,entity.description,entity.watchersCount,entity.stargazersCount,entity.language,entity.forksCount,entity.url)
    }

    override fun mapToEntity(data: Project): ProjectEntity {
        return ProjectEntity(data.id, data.name,data.ownerName,
                data.avatarUrl,data.description,data.watchersCount,data.stargazersCount,data.language,data.forksCount,data.url)
    }
}