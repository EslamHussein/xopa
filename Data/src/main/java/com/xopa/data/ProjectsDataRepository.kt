package com.xopa.data

import com.xopa.data.mapper.ProjectMapper
import com.xopa.data.store.ProjectsDataStoreFactory
import com.xopa.domain.interactor.search.GetProjects
import com.xopa.domain.model.Project
import com.xopa.domain.repository.ProjectsRepository
import io.reactivex.Observable
import javax.inject.Inject

class ProjectsDataRepository @Inject constructor(
        private val projectsMapper: ProjectMapper,
        private val projectsDataStoreFactory: ProjectsDataStoreFactory

) : ProjectsRepository {
    override fun getProjects(params: GetProjects.Params): Observable<List<Project>> {

        return projectsDataStoreFactory.getDataStore().getProjects(params).map { it ->
            it.map {
                projectsMapper.mapFromEntity(it)
            }
        }
    }
}