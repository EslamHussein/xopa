package com.xopa.data.repository

import com.xopa.data.model.ProjectEntity
import com.xopa.domain.interactor.search.GetProjects
import io.reactivex.Observable

interface ProjectsDataStore {

    fun getProjects(params: GetProjects.Params): Observable<List<ProjectEntity>>
}