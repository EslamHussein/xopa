package com.xopa.data.repository

import com.xopa.data.model.ProjectEntity
import io.reactivex.Observable

interface ProjectsRemote {

    fun getProjects(q: String, sort: String, order: String, page: Int): Observable<List<ProjectEntity>>
}