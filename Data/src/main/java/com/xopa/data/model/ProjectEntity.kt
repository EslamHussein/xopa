package com.xopa.data.model

class ProjectEntity(val id: Int, val name: String, val ownerName: String,
                    val avatarUrl: String, val description: String?,
                    val watchersCount: Int, val stargazersCount: Int,
                    val language: String?, val forksCount: Int, val url: String)