package com.xopa.data.store

import com.xopa.data.repository.ProjectsDataStore
import com.xopa.domain.interactor.search.GetProjects
import javax.inject.Inject

open class ProjectsDataStoreFactory @Inject constructor(private val projectsRemoteDataStore: ProjectsRemoteDataStore) {


    fun getDataStore(): ProjectsDataStore {
        return projectsRemoteDataStore
    }
}