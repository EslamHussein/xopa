package com.xopa.data.store

import com.xopa.data.model.ProjectEntity
import com.xopa.data.repository.ProjectsDataStore
import com.xopa.data.repository.ProjectsRemote
import com.xopa.domain.interactor.search.GetProjects
import io.reactivex.Observable
import javax.inject.Inject

open class ProjectsRemoteDataStore @Inject constructor(private val remote: ProjectsRemote) : ProjectsDataStore {
    override fun getProjects(params: GetProjects.Params): Observable<List<ProjectEntity>> {
        return remote.getProjects(params.q, params.sort, params.order, params.page)
    }
}