package com.xopa.data

import com.nhaarman.mockito_kotlin.any
import com.nhaarman.mockito_kotlin.mock
import com.nhaarman.mockito_kotlin.whenever
import com.xopa.data.mapper.ProjectMapper
import com.xopa.data.repository.ProjectsDataStore
import com.xopa.data.store.ProjectsDataStoreFactory
import com.xopa.data.test.factory.ProjectFactory
import io.reactivex.Observable
import org.junit.Before
import org.junit.Test
import org.junit.runner.RunWith
import org.junit.runners.JUnit4

@RunWith(JUnit4::class)
class ProjectsDataRepositoryTest {

    private val projectsMapper = mock<ProjectMapper>()
    private val projectDataStore = mock<ProjectsDataStore>()
    private val projectsDataStoreFactory = mock<ProjectsDataStoreFactory>()
    private val repository = ProjectsDataRepository(projectsMapper, projectsDataStoreFactory)


    @Before
    fun setup() {
        whenever(projectsDataStoreFactory.getDataStore()).thenReturn(projectDataStore)
        whenever(projectDataStore.getProjects(any())).thenReturn(Observable.just(listOf(ProjectFactory.makeProjectEntity())))
    }


    @Test
    fun getProjectsCompletes() {
        whenever(projectsMapper.mapFromEntity(ProjectFactory.makeProjectEntity()))
                .thenReturn(any())
        val testObserver = repository.getProjects(any()).test()
        testObserver.assertComplete()


    }

}
