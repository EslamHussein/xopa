package com.xopa.data.mapper

import com.xopa.data.test.factory.ProjectFactory
import org.junit.Before
import org.junit.Test

import org.junit.Assert.*

class ProjectMapperTest {

    private val mapper = ProjectMapper()

    @Test
    fun mapFromEntity() {

        val entity = ProjectFactory.makeProjectEntity()
        val model = mapper.mapFromEntity(entity)

        assertEquals(entity.id, model.id)
        assertEquals(entity.name, model.name)


    }

    @Test
    fun mapToEntity() {

        val model = ProjectFactory.makeProject()
        val entity = mapper.mapToEntity(model)

        assertEquals(entity.id, model.id)
        assertEquals(entity.name, model.name)

    }
}