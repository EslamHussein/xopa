package com.xopa.data.store

import com.nhaarman.mockito_kotlin.mock
import org.junit.Test

import org.junit.Assert.*
import org.junit.Before

class ProjectsDataStoreFactoryTest {

    private lateinit var remoteDataStore: ProjectsRemoteDataStore
    private lateinit var projectsDataStoreFactory: ProjectsDataStoreFactory

    @Before
    fun setup() {

        remoteDataStore = mock()
        projectsDataStoreFactory = ProjectsDataStoreFactory(remoteDataStore)
    }

    @Test
    fun getDataStoreReturnRemoteDataStore() {
        assertEquals(remoteDataStore, projectsDataStoreFactory.getDataStore())
    }

    @Test
    fun getDataStoreNotNull() {
        assertNotNull(projectsDataStoreFactory.getDataStore())
    }


}