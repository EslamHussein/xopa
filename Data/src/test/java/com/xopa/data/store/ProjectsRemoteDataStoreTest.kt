package com.xopa.data.store

import com.nhaarman.mockito_kotlin.any
import com.nhaarman.mockito_kotlin.mock
import com.nhaarman.mockito_kotlin.whenever
import com.xopa.data.model.ProjectEntity
import com.xopa.data.repository.ProjectsRemote
import com.xopa.data.test.factory.ProjectFactory
import com.xopa.domain.interactor.search.GetProjects
import io.reactivex.Observable
import org.junit.Test

import org.junit.Assert.*
import org.junit.Before
import org.junit.runner.RunWith
import org.junit.runners.JUnit4

@RunWith(JUnit4::class)
class ProjectsRemoteDataStoreTest {

    private val remote = mock<ProjectsRemote>()
    private val remoteDataStore = ProjectsRemoteDataStore(remote)
    private lateinit var projectsResponse: List<ProjectEntity>
    private lateinit var param: GetProjects.Params

    @Before
    fun setup() {
        param = GetProjects.Params.forProjects("android", "a", "a", 1)
        projectsResponse = listOf(ProjectFactory.makeProjectEntity())
        whenever(remote.getProjects(param.q,param.sort,param.order,param.page)).thenReturn(Observable.just(projectsResponse))

    }

    @Test
    fun getProjects() {
        val testObservable = remoteDataStore.getProjects(param).test()
        testObservable.assertComplete()
    }

    @Test
    fun getProjectReturnData() {
        val testObservable = remoteDataStore.getProjects(param).test()
        testObservable.assertValue(projectsResponse)

    }
}