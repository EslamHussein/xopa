package com.xopa.data.test.factory

import com.xopa.data.model.ProjectEntity
import com.xopa.domain.model.Project

object ProjectFactory {

    fun makeProjectEntity(): ProjectEntity = ProjectEntity(DataFactory.randomInt(),
            DataFactory.randomString(),DataFactory.randomString(),DataFactory.randomString(),
            DataFactory.randomString(),DataFactory.randomInt(),DataFactory.randomInt(),
            DataFactory.randomString(),DataFactory.randomInt(),DataFactory.randomString())

    fun makeProject(): Project = Project(DataFactory.randomInt(), DataFactory.randomString(),
            DataFactory.randomString(),DataFactory.randomString(),DataFactory.randomString(),
            DataFactory.randomInt(),DataFactory.randomInt(),DataFactory.randomString(),
            DataFactory.randomInt(),DataFactory.randomString())
}