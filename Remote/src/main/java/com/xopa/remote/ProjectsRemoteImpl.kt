package com.xopa.remote

import com.xopa.data.model.ProjectEntity
import com.xopa.data.repository.ProjectsRemote
import com.xopa.remote.mapper.ProjectsResponseModelMapper
import com.xopa.remote.service.GitHubTrendingService
import io.reactivex.Observable
import javax.inject.Inject

class ProjectsRemoteImpl @Inject constructor(private val service: GitHubTrendingService,
                                             private val mapper: ProjectsResponseModelMapper) : ProjectsRemote {
    override fun getProjects(q: String, sort: String, order: String, page: Int): Observable<List<ProjectEntity>> {

        return service.searchRepositories(q, sort, order, page).map { it ->
            it.projects.map {
                mapper.mapFromModel(it)
            }
        }

    }
}