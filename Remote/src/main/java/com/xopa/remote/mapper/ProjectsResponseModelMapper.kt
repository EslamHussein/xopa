package com.xopa.remote.mapper

import com.xopa.data.model.ProjectEntity
import com.xopa.remote.model.ProjectModel
import javax.inject.Inject

open class ProjectsResponseModelMapper @Inject constructor() : ModelMapper<ProjectModel, ProjectEntity> {
    override fun mapFromModel(model: ProjectModel): ProjectEntity {

        return ProjectEntity(model.id, model.name,model.owner.name,model.owner.avatarUrl
                ,model.description,model.watchersCount,model.stargazersCount,model.language,
                model.forksCount,model.url)
    }
}