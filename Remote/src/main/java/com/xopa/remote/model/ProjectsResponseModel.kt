package com.xopa.remote.model

import com.google.gson.annotations.SerializedName

data class ProjectsResponseModel(@SerializedName("items") val projects: List<ProjectModel>)