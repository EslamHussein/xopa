package com.xopa.remote.service

import com.xopa.remote.model.ProjectsResponseModel
import io.reactivex.Observable
import retrofit2.http.GET
import retrofit2.http.Query

interface GitHubTrendingService {

    @GET("search/repositories")
    fun searchRepositories(@Query("q") query: String, @Query("sort") sort: String
                           , @Query("order") order: String, @Query("page") page: Int)
            : Observable<ProjectsResponseModel>
}