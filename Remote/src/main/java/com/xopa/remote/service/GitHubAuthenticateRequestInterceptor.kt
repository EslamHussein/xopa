package com.xopa.remote.service

import okhttp3.Interceptor
import okhttp3.Response

class GitHubAuthenticateRequestInterceptor : Interceptor {
    override fun intercept(chain: Interceptor.Chain?): Response {

        val original = chain!!.request()

        val request = original.newBuilder()
                .header("Authorization", "token ${CloudConfig.GIT_HUB_TOKEN}")
                .method(original.method(), original.body())
                .build()

        return chain.proceed(request)!!


    }
}




