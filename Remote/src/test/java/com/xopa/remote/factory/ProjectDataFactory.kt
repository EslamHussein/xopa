package com.xopa.remote.factory

import com.xopa.data.model.ProjectEntity
import com.xopa.remote.model.OwnerModel
import com.xopa.remote.model.ProjectModel
import com.xopa.remote.model.ProjectsResponseModel

object ProjectDataFactory {

    fun makeOwner(): OwnerModel = OwnerModel(DataFactory.randomString(),
            DataFactory.randomInt(), DataFactory.randomString())

    fun makeProject(): ProjectModel = ProjectModel(DataFactory.randomInt(),
            DataFactory.randomString(), makeOwner(), DataFactory.randomString(),
            DataFactory.randomInt(), DataFactory.randomInt(), DataFactory.randomString(),
            DataFactory.randomInt(),DataFactory.randomString())

    fun makeProjectEntity(): ProjectEntity = ProjectEntity(DataFactory.randomInt(),
            DataFactory.randomString(),DataFactory.randomString(),DataFactory.randomString(),
            DataFactory.randomString(),DataFactory.randomInt(),DataFactory.randomInt(),
            DataFactory.randomString(),DataFactory.randomInt(),DataFactory.randomString())

    fun makeProjectResponse(): ProjectsResponseModel = ProjectsResponseModel(listOf(makeProject(),
            makeProject(), makeProject()))
}