package com.xopa.remote

import com.nhaarman.mockito_kotlin.any
import com.nhaarman.mockito_kotlin.mock
import com.nhaarman.mockito_kotlin.verify
import com.nhaarman.mockito_kotlin.whenever
import com.xopa.data.model.ProjectEntity
import com.xopa.remote.factory.ProjectDataFactory
import com.xopa.remote.mapper.ProjectsResponseModelMapper
import com.xopa.remote.service.GitHubTrendingService
import io.reactivex.Observable
import org.junit.Test

import org.junit.Assert.*
import org.junit.Before
import org.junit.runner.RunWith
import org.junit.runners.JUnit4


@RunWith(JUnit4::class)
class ProjectsRemoteImplTest {


    private val service = mock<GitHubTrendingService>()
    private val mapper = mock<ProjectsResponseModelMapper>()
    private val remoteImplTest = ProjectsRemoteImpl(service, mapper)

    private val result = ProjectDataFactory.makeProjectResponse()


    @Before
    fun setup() {
        whenever(service.searchRepositories("android", "stars", "desc",0)).thenReturn(Observable.just(result))
        whenever(mapper.mapFromModel(any())).thenReturn(ProjectDataFactory.makeProjectEntity())
    }

    @Test
    fun getProjectsCompletes() {

        val testObservable = remoteImplTest.getProjects("android", "stars", "desc",0).test()
        testObservable.assertComplete()

    }

    @Test
    fun getProjectsAssertsCorrectParameter() {

        remoteImplTest.getProjects("android", "stars", "desc",0).test()

        verify(service).searchRepositories("android", "stars", "desc",0)

    }

    @Test
    fun getProjectsReturnedData() {

        val entities = mutableListOf<ProjectEntity>()

        result.projects.forEach {
            entities.add(mapper.mapFromModel(it))

        }

        val testObservable = remoteImplTest.getProjects("android", "stars", "desc",0).test()
        testObservable.assertValue(entities)

    }

    @Test
    fun getProjectsCallService() {


        remoteImplTest.getProjects("android", "stars", "desc",0).test()

        verify(service).searchRepositories("android", "stars", "desc",0)

    }

}