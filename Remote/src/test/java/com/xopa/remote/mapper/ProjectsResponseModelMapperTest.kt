package com.xopa.remote.mapper

import com.xopa.remote.factory.ProjectDataFactory
import org.junit.Assert.*
import org.junit.Test

class ProjectsResponseModelMapperTest {


    private val mapper = ProjectsResponseModelMapper()
    @Test
    fun mapFromModel() {

        val model = ProjectDataFactory.makeProject()
        val entity = mapper.mapFromModel(model)

        assertEquals(model.id, entity.id)
        assertEquals(model.name, entity.name)
    }


}