package com.xopa.github.injection

import com.nhaarman.mockito_kotlin.mock
import com.xopa.data.repository.ProjectsRemote
import com.xopa.remote.service.GitHubTrendingService
import dagger.Module
import dagger.Provides

@Module
object TestRemoteModule {

    @Provides
    @JvmStatic
    fun provideGithubService(): GitHubTrendingService {
        return mock()
    }

    @Provides
    @JvmStatic
    fun provideProjectsRemote(): ProjectsRemote {
        return mock()
    }

}