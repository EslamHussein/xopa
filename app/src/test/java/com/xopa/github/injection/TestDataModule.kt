package com.xopa.github.injection

import com.nhaarman.mockito_kotlin.mock
import com.xopa.domain.repository.ProjectsRepository
import dagger.Module
import dagger.Provides
import javax.inject.Singleton

@Module
object TestDataModule {

    @Provides
    @JvmStatic
    @Singleton
    fun provideDataRepository(): ProjectsRepository {
        return mock()
    }

}
