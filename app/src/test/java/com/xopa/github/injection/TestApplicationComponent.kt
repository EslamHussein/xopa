package com.xopa.github.injection

import android.app.Application
import com.xopa.domain.repository.ProjectsRepository
import com.xopa.github.injection.module.PresentationModule
import com.xopa.github.injection.module.UIModule
import com.xopa.github.test.TestApplication
import dagger.BindsInstance
import dagger.Component
import dagger.android.support.AndroidSupportInjectionModule
import javax.inject.Singleton

@Singleton
@Component(modules = arrayOf(AndroidSupportInjectionModule::class,
        TestApplicationModule::class,
        TestDataModule::class,
        PresentationModule::class,
        UIModule::class,
        TestRemoteModule::class))
interface TestApplicationComponent {

    fun projectsRepository(): ProjectsRepository

    @Component.Builder
    interface Builder {
        @BindsInstance
        fun application(application: Application): TestApplicationComponent.Builder

        fun build(): TestApplicationComponent
    }

    fun inject(application: TestApplication)

}