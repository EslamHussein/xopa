package com.xopa.github.test.factory

import com.xopa.presentation.model.ProjectView

object ProjectFactory {

    fun makeProjectView(): ProjectView {
        return ProjectView(DataFactory.randomInt(), DataFactory.randomString(),
                DataFactory.randomString(), DataFactory.randomString(), DataFactory.randomString(),
                DataFactory.randomInt(), DataFactory.randomInt(), DataFactory.randomString(), DataFactory.randomInt(), DataFactory.randomString())
    }

}