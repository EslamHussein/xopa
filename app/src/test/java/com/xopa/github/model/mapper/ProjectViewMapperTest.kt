package com.xopa.github.model.mapper

import com.xopa.github.test.factory.ProjectFactory
import org.junit.Test

import org.junit.Assert.*

class ProjectViewMapperTest {
    private val projectMapper = ProjectViewMapper()

    @Test
    fun mapToView() {
        val project = ProjectFactory.makeProjectView()
        val projectForUi = projectMapper.mapToView(project)

        assertEquals(project.id, projectForUi.id)
        assertEquals(project.name, projectForUi.name)
        assertEquals(project.ownerName, projectForUi.ownerName)
        assertEquals(project.avatarUrl, projectForUi.avatarUrl)
        assertEquals(project.description, projectForUi.description)
        assertEquals(project.watchersCount, projectForUi.watchersCount)
        assertEquals(project.watchersCount, projectForUi.watchersCount)
        assertEquals(project.stargazersCount, projectForUi.stargazersCount)
        assertEquals(project.language, projectForUi.language)
        assertEquals(project.forksCount, projectForUi.forksCount)
        assertEquals(project.url, projectForUi.url)

    }
}