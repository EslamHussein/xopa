package com.xopa.github.search

import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import com.bumptech.glide.Glide
import com.bumptech.glide.request.RequestOptions
import com.xopa.github.R
import com.xopa.github.base.AbstractPagingAdapter
import com.xopa.presentation.model.ProjectView



class ProjectsSearchAdapter constructor(private var onProjectClicked: OnProjectClickListener?) : AbstractPagingAdapter<ProjectView, ProjectsSearchAdapter.ViewHolder>() {


    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val itemView = LayoutInflater
                .from(parent.context)
                .inflate(R.layout.project_item_view, parent, false)
        return ViewHolder(itemView)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val project = data[position]
        holder.ownerNameText.text = project.ownerName
        holder.projectNameText.text = project.name

        Glide.with(holder.itemView.context)
                .load(project.avatarUrl)
                .apply(RequestOptions.circleCropTransform())
                .into(holder.avatarImage)


        holder.itemView.setOnClickListener {
            onProjectClicked?.onItemClickListener(project)

        }
    }


    inner class ViewHolder(view: View) : RecyclerView.ViewHolder(view) {
        var avatarImage: ImageView
        var ownerNameText: TextView
        var projectNameText: TextView

        init {
            avatarImage = view.findViewById(R.id.ownerAvatarImageView)
            ownerNameText = view.findViewById(R.id.ownerNameTextView)
            projectNameText = view.findViewById(R.id.projectNameTextView)
        }
    }

    interface OnProjectClickListener {

        fun onItemClickListener(project: ProjectView)
    }
}