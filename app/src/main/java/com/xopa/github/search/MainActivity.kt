package com.xopa.github.search

import android.content.Intent
import android.os.Bundle
import android.os.PersistableBundle
import android.support.v4.app.Fragment
import com.xopa.github.R
import com.xopa.github.projectdetails.ARG_PARAM_PROJECT
import com.xopa.github.projectdetails.ProjectDetailsActivity
import com.xopa.github.projectdetails.ProjectDetailsFragment
import com.xopa.github.util.FragmentUtils
import com.xopa.presentation.model.ProjectView
import dagger.android.support.DaggerAppCompatActivity
import kotlinx.android.synthetic.main.activity_main.*


private const val PROJECT_ARG = "PROJECT_ARG"

class MainActivity : DaggerAppCompatActivity(), ProjectsSearchAdapter.OnProjectClickListener {


    var projectsSearchFragment: Fragment? = null
    var projectDetailsFragment: Fragment? = null
    var selectedProject: ProjectView? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)


        projectsSearchFragment = supportFragmentManager.findFragmentById(R.id.projectsSearchFragment)


        if (savedInstanceState != null) {
            selectedProject = savedInstanceState.getParcelable(PROJECT_ARG)
            if (selectedProject != null)
                onItemClickListener(selectedProject!!)
        }


    }

    override fun onSaveInstanceState(outState: Bundle?) {
        if (selectedProject != null)
            outState?.putParcelable(PROJECT_ARG, selectedProject)

        super.onSaveInstanceState(outState)
    }

    override fun onItemClickListener(project: ProjectView) {
        selectedProject = project
        if (projectDetailsFragmentMainActivity != null) {
            projectDetailsFragment = ProjectDetailsFragment.newInstance(project)
            FragmentUtils.transact(this, projectDetailsFragment!!, R.id.projectDetailsFragmentMainActivity, fragmentTag = "projectDetailsFragment")
        } else {
            val intent = Intent(this, ProjectDetailsActivity::class.java)
            intent.putExtra(ARG_PARAM_PROJECT, project)
            startActivity(intent)

        }
    }

}
