package com.xopa.github.search


import android.content.Context
import android.os.Bundle
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast

import com.xopa.github.R
import com.xopa.github.base.BaseFragment
import com.xopa.github.util.EndlessRecyclerViewScrollListener
import com.xopa.presentation.model.ProjectView
import com.xopa.presentation.projects.ProjectsContract
import kotlinx.android.synthetic.main.fragment_projects_search.*


private const val PROJECTS_LIST_TAG = "PROJECTS_LIST"
private const val FIRST_VISIBLE_CELL_TAG = "FIRST_VISIBLE_CELL"


class ProjectsSearchFragment : BaseFragment<ProjectsContract.ProjectsView, ProjectsContract.ProjectsPresenter>(), ProjectsContract.ProjectsView, ProjectsSearchAdapter.OnProjectClickListener {


    private var projectsAdapter: ProjectsSearchAdapter? = null

    private lateinit var viewManager: LinearLayoutManager
    private var onItemClicked: ProjectsSearchAdapter.OnProjectClickListener? = null


    override fun onAttach(context: Context?) {
        super.onAttach(context)
        onItemClicked = activity as? ProjectsSearchAdapter.OnProjectClickListener

    }


    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        // Inflate the layout for this projectDetailsFragment
        return inflater.inflate(R.layout.fragment_projects_search, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)


        viewManager = LinearLayoutManager(context)

        projectsAdapter = ProjectsSearchAdapter(onProjectClicked = this)

        projectsSearchRecyclerView.apply {
            layoutManager = viewManager
            adapter = projectsAdapter
        }
        projectsSearchSwipeRefresh.isEnabled = false

        projectsSearchRecyclerView.addOnScrollListener(object : EndlessRecyclerViewScrollListener(viewManager) {
            override fun onLoadMore(page: Int, totalItemsCount: Int, view: RecyclerView?) {
                presenter.searchTrending(projectsAdapter?.getNextPageNumber() ?: 0)
            }
        })

    }


    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)

        if (savedInstanceState != null) {
            val projects = savedInstanceState.getParcelableArrayList<ProjectView>(PROJECTS_LIST_TAG)
            val firstVisibleCellIndex = savedInstanceState.getInt(FIRST_VISIBLE_CELL_TAG, 0)
            showSearchResultSuccess(projects!!, firstVisibleCellIndex = firstVisibleCellIndex)
        } else {
            presenter.searchTrending(projectsAdapter?.getNextPageNumber() ?: 0)

        }

    }


    override fun onSaveInstanceState(outState: Bundle) {

        outState.putParcelableArrayList(PROJECTS_LIST_TAG, projectsAdapter?.getItems())
        outState.putInt(FIRST_VISIBLE_CELL_TAG, viewManager.findFirstVisibleItemPosition())
        super.onSaveInstanceState(outState)
    }

    override fun showLoading() {

        projectsSearchSwipeRefresh.isRefreshing = true
        if (projectsAdapter?.itemCount ?: 0 <= 0) {
            loadingStatusTextView.text = getString(R.string.loading)
        } else {
            loadingStatusTextView.visibility = View.GONE
        }
    }

    override fun showSearchResultSuccess(projects: List<ProjectView>, firstVisibleCellIndex: Int) {

        loadingStatusTextView.visibility = View.GONE

        projectsAdapter?.addItems(projects)

    }


    override fun showSearchResultFailure(msg: String) {
        if (projectsAdapter?.itemCount ?: 0 <= 0) {
            loadingStatusTextView.text = msg
            loadingStatusTextView.visibility = View.VISIBLE

        } else {
            Toast.makeText(context, msg, Toast.LENGTH_LONG).show()

        }
    }

    override fun hideLoading() {
        projectsSearchSwipeRefresh.isRefreshing = false
    }

    override fun onItemClickListener(project: ProjectView) {
        onItemClicked?.onItemClickListener(project)

    }


    override fun onDetach() {
        super.onDetach()
        onItemClicked = null
    }

}
