package com.xopa.github.injection.module

import com.xopa.presentation.projects.ProjectsContract
import com.xopa.presentation.projects.ProjectsPresenterImpl
import dagger.Binds
import dagger.Module

@Module
abstract class PresentationModule {

    @Binds
    abstract fun bindProjectsPresenter(presenter:ProjectsPresenterImpl): ProjectsContract.ProjectsPresenter
}