package com.xopa.github.injection.module

import com.xopa.domain.executer.ExecutionThread
import com.xopa.github.search.MainActivity
import com.xopa.github.UIExecutor
import com.xopa.github.search.ProjectsSearchFragment
import dagger.Binds
import dagger.Module
import dagger.android.ContributesAndroidInjector


@Module
abstract class UIModule {

    @Binds
    abstract fun bindExecutionThread(uiExecutor: UIExecutor): ExecutionThread


    @ContributesAndroidInjector
    abstract fun contributesMainActivity(): MainActivity

    @ContributesAndroidInjector
    internal abstract fun contributeProjectsSearchFragment(): ProjectsSearchFragment



}