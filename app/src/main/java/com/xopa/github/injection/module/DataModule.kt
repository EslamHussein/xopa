package com.xopa.github.injection.module

import com.xopa.data.ProjectsDataRepository
import com.xopa.domain.repository.ProjectsRepository
import dagger.Binds
import dagger.Module

@Module
abstract class DataModule {

    @Binds
    abstract fun bindDataRepository(s: ProjectsDataRepository): ProjectsRepository
}