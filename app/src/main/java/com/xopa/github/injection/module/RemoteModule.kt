package com.xopa.github.injection.module

import com.xopa.data.repository.ProjectsRemote
import com.xopa.presentation.BuildConfig
import com.xopa.remote.ProjectsRemoteImpl
import com.xopa.remote.service.GitHubTrendingService
import com.xopa.remote.service.GitHubTrendingServiceFactory
import dagger.Binds
import dagger.Module
import dagger.Provides

@Module
abstract class RemoteModule {

    @Module
    companion object {
        @Provides
        @JvmStatic
        fun provideGithubService(): GitHubTrendingService {
            return GitHubTrendingServiceFactory.provideGithubTrendingService(BuildConfig.DEBUG)
        }
    }

    @Binds
    abstract fun bindProjectsRemote(projectsRemote: ProjectsRemoteImpl): ProjectsRemote

}