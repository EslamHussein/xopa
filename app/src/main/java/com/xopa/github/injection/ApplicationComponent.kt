package com.xopa.github.injection

import android.app.Application
import com.xopa.github.App
import com.xopa.github.injection.module.*
import dagger.BindsInstance
import dagger.Component
import dagger.android.AndroidInjectionModule
import dagger.android.AndroidInjector
import dagger.android.support.DaggerApplication
import javax.inject.Singleton


@Singleton
@Component(modules = [(AndroidInjectionModule::class), (ApplicationModule::class),
    (UIModule::class), (PresentationModule::class), (DataModule::class), (RemoteModule::class)])
interface ApplicationComponent {

    @Component.Builder
    interface Builder {
        @BindsInstance
        fun application(application: Application): Builder

        fun build(): ApplicationComponent
    }

    fun inject(app: App)

}