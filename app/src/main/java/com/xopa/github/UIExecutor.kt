package com.xopa.github

import com.xopa.domain.executer.ExecutionThread
import io.reactivex.Scheduler
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers
import javax.inject.Inject

class UIExecutor @Inject constructor() :ExecutionThread {
     override val subscribeScheduler: Scheduler
        get() = Schedulers.io()
     override val observerScheduler: Scheduler
        get() = AndroidSchedulers.mainThread()
}

