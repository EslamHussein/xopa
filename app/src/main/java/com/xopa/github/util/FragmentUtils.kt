package com.xopa.github.util

import android.support.annotation.IdRes
import android.support.v4.app.Fragment
import android.support.v7.app.AppCompatActivity


enum class FragmentTransaction {
    ADD, Replace
}

object FragmentUtils {


    fun transact(activity: AppCompatActivity, mFragment: Fragment, @IdRes container: Int,
                 isNeedToAddToStack: Boolean = false, transaction: FragmentTransaction = FragmentTransaction.Replace,
                 fragmentTag: String? = null) {
        var fragment = mFragment

        val fragTag = fragmentTag ?: fragment::class.simpleName
        val manager = activity.supportFragmentManager
        val isInStack = manager.popBackStackImmediate(fragTag, 0)
        val ft = manager.beginTransaction()

        if (isInStack) {
            fragment = manager.findFragmentByTag(fragTag)!!
        }
        when (transaction) {

            FragmentTransaction.ADD -> ft.add(container, fragment, fragTag)
            FragmentTransaction.Replace -> ft.replace(container, fragment, fragTag)

        }

        if (!isInStack && isNeedToAddToStack) {
            ft.addToBackStack(fragTag)
        }
        ft.commit()
    }

}