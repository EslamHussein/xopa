package com.xopa.github.model.mapper

import com.xopa.github.model.Project
import com.xopa.presentation.model.ProjectView
import javax.inject.Inject

class ProjectViewMapper @Inject constructor() : ViewMapper<ProjectView, Project> {

    override fun mapToView(presentation: ProjectView): Project {
        return Project(presentation.id, presentation.name,presentation.ownerName,
                presentation.avatarUrl,presentation.description,presentation.watchersCount,
                presentation.stargazersCount,presentation.language,presentation.forksCount,
                presentation.url)
    }

}
