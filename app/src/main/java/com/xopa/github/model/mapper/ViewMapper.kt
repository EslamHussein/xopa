package com.xopa.github.model.mapper

interface ViewMapper<in P, out V> {

    fun mapToView(presentation: P): V
}
