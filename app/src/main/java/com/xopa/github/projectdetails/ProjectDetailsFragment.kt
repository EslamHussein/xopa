package com.xopa.github.projectdetails


import android.os.Bundle
import android.support.v4.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.bumptech.glide.Glide
import com.bumptech.glide.request.RequestOptions

import com.xopa.github.R
import com.xopa.presentation.model.ProjectView
import kotlinx.android.synthetic.main.fragment_project_details.*
import kotlinx.android.synthetic.main.include_project_about.*
import kotlinx.android.synthetic.main.include_statistics.*

const val ARG_PARAM_PROJECT = "Project"

class ProjectDetailsFragment : Fragment() {

    var project: ProjectView? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        arguments?.let {
            project = it.getParcelable(ARG_PARAM_PROJECT)
        }
    }


    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        // Inflate the layout for this projectDetailsFragment
        return inflater.inflate(R.layout.fragment_project_details, container, false)
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)

        if (savedInstanceState != null) {
            project = savedInstanceState.getParcelable(ARG_PARAM_PROJECT)
        }
        updateFragment(project)


    }

    override fun onSaveInstanceState(outState: Bundle) {
        if (project != null)
            outState.putParcelable(ARG_PARAM_PROJECT, project)
        super.onSaveInstanceState(outState)
    }

    fun updateFragment(project: ProjectView?) {
        this.project = project
        ownerNameTextView.text = project?.ownerName
        projectNameTextView.text = project?.name
        projectDescriptionTextView.text = project?.description ?: ""
        watcherTextView.text = (project?.watchersCount ?: 0).toString()
        starTextView.text = (project?.stargazersCount ?: 0).toString()
        Glide.with(context)
                .load(project?.avatarUrl)
                .apply(RequestOptions.circleCropTransform())
                .into(projectImageView)

        projectUrlWebView.loadUrl(project?.url)
    }


    companion object {
        @JvmStatic
        fun newInstance(project: ProjectView) =
                ProjectDetailsFragment().apply {
                    arguments = Bundle().apply {
                        putParcelable(ARG_PARAM_PROJECT, project)
                    }
                }
    }
}