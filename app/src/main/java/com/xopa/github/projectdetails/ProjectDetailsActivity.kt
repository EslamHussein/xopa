package com.xopa.github.projectdetails

import android.content.res.Configuration
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.support.v4.app.Fragment
import com.xopa.github.R
import com.xopa.presentation.model.ProjectView

const val Project_DETAILS_FRAGMENT_TAG = "ProjectDetailsFragment"


class ProjectDetailsActivity : AppCompatActivity() {
    var projectDetailsFragment: Fragment? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_project_details)


        if (resources.getConfiguration().orientation == Configuration.ORIENTATION_LANDSCAPE)
            finish()

        supportActionBar?.setDisplayShowHomeEnabled(true)
        supportActionBar?.setDisplayHomeAsUpEnabled(true)

        val project: ProjectView = intent.getParcelableExtra(ARG_PARAM_PROJECT)

        projectDetailsFragment = supportFragmentManager.findFragmentById(R.id.projectDetailsFragment)


        if (projectDetailsFragment != null && projectDetailsFragment is ProjectDetailsFragment)
            (projectDetailsFragment as ProjectDetailsFragment).updateFragment(project)


    }

    override fun onSaveInstanceState(outState: Bundle) {
        super.onSaveInstanceState(outState)

        supportFragmentManager.putFragment(outState, Project_DETAILS_FRAGMENT_TAG, projectDetailsFragment!!)
    }

    override fun onSupportNavigateUp(): Boolean {
        onBackPressed()
        return true
    }
}
